#include <QDebug>
#include <iostream>
#include <fstream>
#include <time.h>

#ifdef __MINGW32__
#include <windows.h>
#endif

#include "project.h"
#include "exporter.h"

using namespace std;

void Exporter::exportify(std::string path, Project *pro)
{
	// package.json

	ofstream pf;

	pf.open(path + "/package.json");
	pf << "{" << std::endl;
	pf << "	\"id\" : \"" << pro->name << "\"," << endl;
	pf << "	\"name\" : \"" << pro->name << "\"," << endl;
	pf << "	\"version\" : \"" << pro->version << "\"," << endl;
	pf << "	\"author\" : \"" << pro->author << "\"," << endl;
	pf << "	\"url\" : \"" << pro->url << "\"," << endl;
	pf << "	\"description\" : \"" << pro->desc << "\"," << endl;
	pf << "	\"main\" : \"./main.js\"," << endl;
	pf << "	\"dependencies\" : {" << endl;
	pf << "		\"gdt-modAPI\" : \"0.1.x\"" << endl;
	pf << "	}" << endl;
	pf << "}" << endl;

	pf.close();

	// main.js

	ofstream mf;

	string globalVar = to_string(time(NULL));

	mf.open(path + "/main.js");
	mf << globalVar << " = {};" << endl;
	mf << endl;
	mf << "(function(){" << endl;
	mf << "	" << pro->name << ".path = GDT.getRelativePath();" << endl;
	mf << endl;
	mf << "	var ready = function (){" << endl;
	mf << "		console.log('Loaded " << pro->name << "');" << endl;
	mf << "	};" << endl;
	mf << endl;
	mf << "	var error = function(){" << endl;
	mf << "		console.log('Failed to load " << pro->name << "');" << endl;
	mf << "	};" << endl;
	mf << endl;
	mf << "	GDT.loadJs([" << globalVar << ".path + '/src/topics.js'], ready, error);" << endl;
	mf << "})();" << endl;

	// src/topics.js

	#ifdef __MINGW32__
	string command = path + "/src";
	CreateDirectory (command.c_str(), NULL);
	#else
	string command = "mkdir " + path + "/src";
	system(command.c_str());
	#endif

	ofstream tf;

	tf.open(path + "/src/topics.js");

	tf << "(function(){" << endl;
	tf << "	" << endl;
	tf << "	GDT.addTopics([" << endl;

	for(int i = 0; i < pro->topics.size(); i++)
	{
		tf << "	{" << endl;
		tf << "		id: \"" << pro->topics[i].name << "\"," << endl;
		tf << "		name: \"" << pro->topics[i].name << "\"," << endl;
		tf << "		genreWeightings: [" <<
					to_string(pro->topics[i].acw) <<
					", " <<
					to_string(pro->topics[i].adw) <<
					", " <<
					to_string(pro->topics[i].rpw) <<
					", " <<
					to_string(pro->topics[i].stw) <<
					", " <<
					to_string(pro->topics[i].siw) <<
					", " <<
					to_string(pro->topics[i].caw) <<
					"]," << endl;
		tf << "		audienceWeightings: [" <<
		to_string(pro->topics[i].young) <<
		", " <<
		to_string(pro->topics[i].everyone) <<
		", " <<
		to_string(pro->topics[i].mature) <<
		"]" << endl;

		if(i == pro->topics.size() - 1)
		{
			tf << "	}" << endl;
		}else{
			tf << "	}," << endl;
		}
	}

	tf << "	]);" << endl;
	tf << "	" << endl;
	tf << "})();" << endl;

	mf.close();
}
