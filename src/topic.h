#ifndef TOPIC_H
#define TOPIC_H

#include <string>

class Topic
{
public:
	std::string name;
	double acw;
	double adw;
	double rpw;
	double stw;
	double siw;
	double caw;

	double young;
	double everyone;
	double mature;

	Topic(std::string n,
		double ac,
		double ad,
		double rp,
		double st,
		double si,
		double ca,
		double young,
		double everyone,
		double mature);
	Topic();
	~Topic();

};

#endif
