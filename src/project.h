#ifndef PROJECT_H
#define PROJECT_H

#include <string>
#include <vector>
#include "topic.h"

class Project
{
public:
	std::string name = "Name";
	std::string author = "Author";
	std::string version = "0.10";
	std::string url = "";
	std::string desc = "Description";

	std::vector<Topic> topics;

	Project();
	~Project();
	int LoadFromFile(std::string path);
	int SaveToFile(std::string path);
};

#endif
