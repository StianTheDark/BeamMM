#include <QApplication>
#include <QStringListModel>
#include <QStringList>
#include <QMainWindow>
#include <QDebug>
#include <QDialog>
#include <QSignalMapper>
#include <QFileDialog>
#include <QPixmap>
#include <QModelIndex>
#include <QMessageBox>

#include <iostream>
#include <chrono>
#include <thread>
#include <stdlib.h>

#include "exporter.h"

#include "project.h"
#include "ui_Window.h"
#include "ui_TopicDialog.h"

using namespace std;

void onTopicsClick(QModelIndex index);

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	QMainWindow *window = new QMainWindow;
	Ui::MainWindow ui;

	ui.setupUi(window);

	Project *currentPro = new Project();

	window->show();

	string startMsg = "\033[32mLog:\033[0m Starting Beam MM...";
	string exportMsg = "\033[32mLog:\033[0m Exporting...";

	qDebug(startMsg.c_str());

	Exporter *ex;

	// Toolbar
	QObject::connect (ui.actionExit, &QAction::triggered, window, &QMainWindow::close);

	QObject::connect (ui.actionLoad, &QAction::triggered, window, [currentPro, window]{
		QFileDialog dialog(window);
		QStringList file;

		if(dialog.exec())
		{
			file = dialog.selectedFiles();
		}else{
			return;
		}

		currentPro->LoadFromFile(file.at(0).toUtf8().constData());
	});

	QObject::connect (ui.actionExport, &QAction::triggered, [ex, currentPro, window, exportMsg]{
		QFileDialog dialog(window);
		dialog.setFileMode(QFileDialog::Directory);
		dialog.setOption(QFileDialog::ShowDirsOnly);

		QStringList file;
		if(dialog.exec())
		{
			file = dialog.selectedFiles();
			qDebug(exportMsg.c_str());
		}else{
			return;
		}

		ex->exportify(file.at(0).toUtf8().constData(), currentPro);
	});

	// Topics List
	QStringListModel *model = new QStringListModel(window);
	QStringList list;
	model->setStringList(list);
	ui.topicsList->setModel(model);

	// Project Tab
	QObject::connect (ui.nameBox, &QLineEdit::textChanged, [currentPro, ui]{
		currentPro->name = ui.nameBox->text().toUtf8().constData();
	});

	QObject::connect (ui.authorBox, &QLineEdit::textChanged, [currentPro, ui]{
		currentPro->author = ui.authorBox->text().toUtf8().constData();
	});

	QObject::connect (ui.versionSpinner, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [currentPro, ui]{
		currentPro->version = ui.versionSpinner->text().toUtf8().constData();
	});

	QObject::connect (ui.urlBox, &QLineEdit::textChanged, [currentPro, ui]{
		currentPro->url = ui.urlBox->text().toUtf8().constData();
	});

	QObject::connect (ui.descriptionBox, &QPlainTextEdit::textChanged, [currentPro, ui]{
		currentPro->desc = ui.descriptionBox->toPlainText().toUtf8().constData();
	});

	// Add topic button
	QObject::connect(ui.addTopicBtn,  &QPushButton::clicked, [model, currentPro]{
		QDialog *tdDialog = new QDialog;

		Ui::TopicDialog td_ui;
		td_ui.setupUi(tdDialog);
		tdDialog->setModal(true);
		
		td_ui.actionPic->setPixmap(QString(":/res/action.png"));
		td_ui.adventurePic->setPixmap(QString(":/res/adventure.png"));
		td_ui.rpgPic->setPixmap(QString(":/res/rpg.png"));
		td_ui.simulationPic->setPixmap(QString(":/res/simulation.png"));
		td_ui.strategyPic->setPixmap(QString(":/res/strategy.png"));
		td_ui.casualPic->setPixmap(QString(":/res/casual.png"));
		
		int returned = tdDialog->exec();

		if(returned == 1)
		{
			if(td_ui.nameBox->text().toUtf8().constData() == ""){
				return 0;
			}

			QStringList tempList = model->stringList();
			tempList.append(td_ui.nameBox->text().toUtf8().constData());
			model->setStringList(tempList);

			currentPro->topics.push_back(Topic(
				td_ui.nameBox->text().toUtf8().constData(),
				td_ui.actionSpinner->value(),
				td_ui.adventureSpinner->value(),
				td_ui.rpgSpinner->value(),
				td_ui.stratSpinner->value(),
				td_ui.simSpinner->value(),
				td_ui.casSpinner->value(),
				td_ui.youngSpinner->value(),
				td_ui.everySpinner->value(),
				td_ui.matSpinner->value()));
		}else if(returned == 0){
			// Canceled
		}

		delete(tdDialog);
	});

	QObject::connect(ui.editTopicBtn,  &QPushButton::clicked, [model, currentPro, ui]{
		QDialog *tdDialog = new QDialog;

		Ui::TopicDialog td_ui;
		td_ui.setupUi(tdDialog);
		tdDialog->setModal(true);
		td_ui.removeBtn->setEnabled(true);


		tdDialog->setWindowTitle("Edit Topic");

		int pos = ui.topicsList->currentIndex().row();

		QObject::connect(td_ui.removeBtn, &QPushButton::clicked, [tdDialog, model, pos, currentPro]{
			QStringList tempList = model->stringList();

			tempList.removeAt(pos);
			tdDialog->close();

			int newPos = pos;
			currentPro->topics.erase(currentPro->topics.cbegin() + pos);

			model->setStringList(tempList);
		});

		td_ui.nameBox->setText(currentPro->topics[pos].name.c_str());
		td_ui.actionSpinner->setValue(currentPro->topics[pos].acw);
		td_ui.adventureSpinner->setValue(currentPro->topics[pos].adw);
		td_ui.rpgSpinner->setValue(currentPro->topics[pos].rpw);
		td_ui.stratSpinner->setValue(currentPro->topics[pos].stw);
		td_ui.simSpinner->setValue(currentPro->topics[pos].siw);
		td_ui.casSpinner->setValue(currentPro->topics[pos].caw);

	 	td_ui.youngSpinner->setValue(currentPro->topics[pos].young);
		td_ui.everySpinner->setValue(currentPro->topics[pos].everyone);
		td_ui.matSpinner->setValue(currentPro->topics[pos].mature);


		int returned = tdDialog->exec();

		if(returned == 1)
		{
			QStringList tempList = model->stringList();

			for(int i = 0; i < tempList.size(); i++){

				if(tempList.at(i).toUtf8().constData() == currentPro->topics[ui.topicsList->currentIndex().row()].name){
					tempList.replace(i, td_ui.nameBox->text().toUtf8().constData());
					currentPro->topics[pos].name = td_ui.nameBox->text().toUtf8().constData();
					currentPro->topics[pos].acw = td_ui.actionSpinner->value();
					currentPro->topics[pos].adw = td_ui.adventureSpinner->value();
					currentPro->topics[pos].rpw = td_ui.rpgSpinner->value();
					currentPro->topics[pos].stw = td_ui.stratSpinner->value();
					currentPro->topics[pos].siw = td_ui.simSpinner->value();
					currentPro->topics[pos].caw = td_ui.casSpinner->value();

					currentPro->topics[pos].young = td_ui.youngSpinner->value();
					currentPro->topics[pos].everyone = td_ui.everySpinner->value();
					currentPro->topics[pos].mature = td_ui.matSpinner->value();
				}
			}

			model->setStringList(tempList);
		}else if(returned == 0){
			// Canceled
		}

		delete(tdDialog);
	});

	return app.exec();
}
