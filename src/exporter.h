#ifndef EXPORTER_H
#define EXPORTER_H

#include "project.h"

class Exporter
{
public:
	void exportify(std::string path, Project *pro);
};

#endif
